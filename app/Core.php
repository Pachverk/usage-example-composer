<?php


namespace Pachverk\Composer;


class Core
{
    public static function init()
    {
        echo 'Run Init test !!!' . PHP_EOL;
    }

    public static function test($arg='') {
        echo "TEST: ($arg)".PHP_EOL;
    }

    public static function install($arg = '')
    {
        echo "Run install test ($arg) !!!" . PHP_EOL;
    }

    public static function update($arg = '')
    {
        echo "Run update test ($arg) !!!" . PHP_EOL;
    }
}