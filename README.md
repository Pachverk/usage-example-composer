# Usage example Composer

##### Чистый пример для быстрого формирования новых проектов
Важный момент для примеров ниже, `repositories` необходим для других ресурсов которые  
должны быть доступны для подключения через `require`
Для подключения добавьте в файл composer.json:  
```
{
    "repositories": [
        {
            "type": "gitlab",
            "url": "https://gitlab.com/Pachverk/usage-example-composer.git"
        }
    ],
    "require": {
        "pachverk/usage-example-composer" : "*"
    }
}
```
Или этот вариант тоже рабочий:
```
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "pachverk/usage-example-composer",
                "version": "0.0.1",
                "type": "package",
                "source": {
                    "url": "git@gitlab.com:Pachverk/usage-example-composer.git",
                    "type": "git",
                    "reference": "master"
                }
            }
        }
    ],
```